package it.uniba.gol;

public class CustomLifeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public CustomLifeException(String message) {
		super(message);
	}

}
